<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Hidden;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;

class Article extends Resource
{
    public static $model = \App\Models\Article::class;

    public static $title = 'title';

    public static $search = [
        'title',
        'excerpt',
        'text'
    ];

    public static function label(): string
    {
        return __('Articles');
    }

    public static function singularLabel(): string
    {
        return __('Article');
    }

    public function fields(Request $request)
    {
        return [
            Text::make(__('Title'), 'title')
                ->sortable()
                ->rules('required', 'max:255'),

            Trix::make(__('Excerpt'), 'excerpt')
                ->alwaysShow()
                ->sortable()
                ->rules('required'),

            Trix::make(__('Text'), 'text')
                ->alwaysShow()
                ->sortable()
                ->rules('required'),

            BelongsTo::make(__('Author'), 'author', User::class)
                ->sortable()
                ->exceptOnForms(),

            Hidden::make('author_id')
                ->default(fn(Request $request) => $request->user()->id),
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
