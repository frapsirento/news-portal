<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Article extends Model
{
    protected $fillable = [
        'title',
        'excerpt',
        'text',
        'author_id',
    ];

    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
